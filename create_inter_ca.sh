#!/bin/bash -e

version=`date -u +"%Y%m%d_%H%M%S"`

#
# ca/inter
# ├── ca.cert.pem
# ├── ca.crl.pem
# ├── ca.csr.pem
# ├── ca.key.pem
# ├── ca_chain.cert.pem
# ├── crlnumber
# ├── crlnumber.old
# ├── index.txt
# ├── newcerts
# └── serial
#

# create necessary file and directory
mkdir -p ca/inter/ ca/inter/newcerts

if [ ! -f ca/inter/index.txt ]; then
  touch ca/inter/index.txt
fi
if [ ! -f ca/inter/serial ]; then
  echo 1000 > ca/inter/serial
fi
if [ ! -f ca/inter/crlnumber ]; then
  echo 1000 > ca/inter/crlnumber
fi

# create key and csr
echo "create private key and csr"
openssl req \
  -newkey rsa:2048 \
  -keyout ca/inter/ca.key.pem \
  -out ca/inter/ca.csr.pem \
  -subj "/CN=inter_ca.$version/OU=dp/O=appier/L=na/ST=na/C=tw" \
  -nodes

# sign certificate by root ca
echo "sign certificate by root ca"
openssl ca \
  -config root.openssl.cnf \
  -keyfile ca/root/ca.key.pem \
  -cert ca/root/ca.cert.pem \
  -in ca/inter/ca.csr.pem \
  -out ca/inter/ca.cert.pem \
  -days 720 \
  -extensions v3_intermediate_ca

# create an empty crl file
echo "create an empty crl file"
openssl ca \
  -config inter.openssl.cnf \
  -gencrl \
  -keyfile ca/inter/ca.key.pem \
  -cert ca/inter/ca.cert.pem \
  -out ca/inter/ca.crl.pem

# create certificate chain
cat ca/inter/ca.cert.pem ca/root/ca.cert.pem > ca/inter/ca_chain.cert.pem
