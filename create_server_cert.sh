#!/bin/bash -e

version=`date -u +"%Y%m%d_%H%M%S"`

mkdir -p server

# create key and csr
echo "create private key and csr"
openssl req \
  -new \
  -keyout server/server.key.pem \
  -out server/server.csr.pem \
  -subj "/CN=server.$version/OU=dp/O=appier/L=na/ST=na/C=tw" \
  -nodes

# sign certificate by ca
echo "sign certificate by ca"
openssl ca \
  -config inter.openssl.cnf \
  -keyfile ca/inter/ca.key.pem \
  -cert ca/inter/ca.cert.pem \
  -in server/server.csr.pem \
  -out server/server.cert.pem \
  -days 720 \
  -extensions server_cert

# create certificate chain
cat server/server.cert.pem ca/inter/ca_chain.cert.pem > server/ca_chain.cert.pem

