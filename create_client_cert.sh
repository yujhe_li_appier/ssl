#!/bin/bash -e

version=`date -u +"%Y%m%d_%H%M%S"`

#
# client
# ├── ca_chain.cert.pem
# ├── client.cert.pem
# ├── client.csr.pem
# └── client.key.pem
#

mkdir -p client

# create key and csr
echo "create private key and csr"
openssl req \
  -new \
  -keyout client/client.key.pem \
  -out client/client.csr.pem \
  -subj "/CN=client.$version/OU=dp/O=appier/L=na/ST=na/C=tw" \
  -nodes

# sign certificate by ca
echo "sign certificate by ca"
openssl ca \
  -config inter.openssl.cnf \
  -keyfile ca/inter/ca.key.pem \
  -cert ca/inter/ca.cert.pem \
  -in client/client.csr.pem \
  -out client/client.cert.pem \
  -days 720 \
  -extensions usr_cert

# create certificate chain
cat client/client.cert.pem ca/inter/ca_chain.cert.pem > client/ca_chain.cert.pem
