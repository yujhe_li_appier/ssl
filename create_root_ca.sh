#!/bin/bash -e

version=`date -u +"%Y%m%d_%H%M%S"`

#
# ca/root
# ├── ca.cert.pem
# ├── ca.key.pem
# ├── crlnumber
# ├── index.txt
# ├── newcerts
# └── serial
#

# create necessary file and directory
mkdir -p ca/root ca/root/newcerts 

if [ ! -f ca/root/index.txt ]; then
  touch ca/root/index.txt
fi
if [ ! -f ca/root/serial ]; then
  echo 1000 > ca/root/serial
fi
if [ ! -f ca/root/crlnumber ]; then
  echo 1000 > ca/root/crlnumber
fi

# create private key and ca
echo "create private key and ca"
openssl req \
  -config root.openssl.cnf \
  -new \
  -x509 \
  -keyout ca/root/ca.key.pem \
  -out ca/root/ca.cert.pem \
  -days 3650 \
  -subj "/CN=root_ca.${version}/OU=dp/O=appier/L=na/ST=na/C=tw" \
  -nodes
