#!/bin/bash -e

openssl verify \
  -crl_check \
  -CAfile <(cat ca/inter/ca_chain.cert.pem ca/inter/ca.crl.pem) \
  client/client.cert.pem
