#!/bin/bash -e

# revoke client certificate
echo "revoke client certificate"
openssl ca \
  -config inter.openssl.cnf \
  -keyfile ca/inter/ca.key.pem \
  -cert ca/inter/ca.cert.pem \
  -revoke client/client.cert.pem

# re-create crl file
echo "re-create crl file"
openssl ca \
  -config inter.openssl.cnf \
  -gencrl \
  -keyfile ca/inter/ca.key.pem \
  -cert ca/inter/ca.cert.pem \
  -out ca/inter/ca.crl.pem

